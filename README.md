# Graphical Password Authentication

### Submitted by:

 - Aakash Dinkar - 19MCMC62
 - Saurabh Kumar - 19MCMC17
 - Amandeep Singh - 19MCMC14
 - Vani Gupta - 19MCMC55

### What is it? 

Graphics based password authentication is a type of authentication where the traditional system of password or PIN based authentication is replaced by graphical components, such as images. This in turn prevents various classes of attacks like brute force, shoulder surfing and social engineering. 

Close to 91% passwords can be hacked in less than 6 hours using dictionary based attacks. Also, 59% of people use the same password everywhere, therefore, this type of authentication will help to overcome the above mentioned security issues.

This project presents the user few pictures, which the user chooses (not actually the pictures but the locations of those pictures) and uses it subsequently to login.
The next time user tries to login, the pictures shuffle places and the user is required to only click on the places rather than the images.

### Tech Stack:

- Python
- Django
- Boostrap

### How to run:

- First install the requirements:
`pip install requirements.txt`

- Change the current directory so that you are inside the project folder & Run the Django server:
`python manage.py runserver`

- Open web browser and enter the following to use it:
`http://127.0.0.1:8000/`

- To use the reset password feature, admin has to save their email and password in environment variable. Can be acheived as: 
`SET USER_EMAIL=admin@something.com` & `SET USER_PASS=password`

- The reset password link is one time clickable to prevent multiple login attempts.

### Features

- Registration
- Login
- Reset password

### How does it look?

![](/gpa.jpeg)
